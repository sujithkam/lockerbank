﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using LockerBank.Models;

namespace LockerBank.Interface
{
    public interface IDbContext : IDisposable
    {
        DbSet<Region> Regions { get; set; }

        DbSet<Bank> Banks { get; set; }

        DbSet<Locker> Lockers { get; set; }

        void EnsureDbCreated();
    }
}
