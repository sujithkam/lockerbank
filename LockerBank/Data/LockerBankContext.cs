﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using LockerBank.Interface;
using LockerBank.Models;

namespace LockerBank.Data
{
    public class LockerBankContext : DbContext, IDbContext
    {
        public LockerBankContext(DbContextOptions options) : base(options)
        {
            
        }

        public virtual DbSet<Region> Regions { get; set; }
        public virtual DbSet<Bank> Banks { get; set; }
        public virtual DbSet<Locker> Lockers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=LockerBank;Trusted_Connection=True;ConnectRetryCount=0");
            }
        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Region>(entity =>
            {
                entity.Property(e => e.Id).IsRequired();
            });

            #region Region Data Seed
            modelBuilder.Entity<Region>().HasData(new Region { Id = 1, Name = "Sydney", IsActive = true });
            modelBuilder.Entity<Region>().HasData(new Region { Id = 2, Name = "Melbourne", IsActive = true });
            modelBuilder.Entity<Region>().HasData(new Region { Id = 3, Name = "Brisbane", IsActive = true });
            modelBuilder.Entity<Region>().HasData(new Region { Id = 4, Name = "Adelaide", IsActive = true });
            modelBuilder.Entity<Region>().HasData(new Region { Id = 5, Name = "Perth", IsActive = true });
            #endregion

            #region Locker Bank Data Seed

            modelBuilder.Entity<Bank>().HasData(new Bank() { Id = 101, Name = "cbd_lockerbank_1", IsActive = true, RegionId = 1 });
            modelBuilder.Entity<Bank>().HasData(new Bank() { Id = 102, Name = "cbd_lockerbank_2", IsActive = true, RegionId = 1 });
            modelBuilder.Entity<Bank>().HasData(new Bank() { Id = 103, Name = "manly_lockerbank_1", IsActive = true, RegionId = 1 });

            modelBuilder.Entity<Bank>().HasData(new Bank() { Id = 201, Name = "cbd_mlb_lockerbank_1", IsActive = true, RegionId = 2 });
            modelBuilder.Entity<Bank>().HasData(new Bank() { Id = 202, Name = "cbd_mlb_lockerbank_2", IsActive = true, RegionId = 2 });

            modelBuilder.Entity<Bank>().HasData(new Bank() { Id = 301, Name = "cbd_bsb_lockerbank_1", IsActive = true, RegionId = 3 });
            modelBuilder.Entity<Bank>().HasData(new Bank() { Id = 302, Name = "cbd_bsb_lockerbank_2", IsActive = true, RegionId = 3 });
            #endregion

            #region Locker Data Seed

            modelBuilder.Entity<Locker>().HasData(new Locker() { Id = 10001, Name = "L1", LockerKey = "1234", IsActive = true, IsOpen = false, BankId = 101 });
            modelBuilder.Entity<Locker>().HasData(new Locker() { Id = 10002, Name = "L2", LockerKey = "1234", IsActive = true, IsOpen = false, BankId = 101 });
            modelBuilder.Entity<Locker>().HasData(new Locker() { Id = 10003, Name = "L3", LockerKey = "1234", IsActive = true, IsOpen = false, BankId = 102 });
            modelBuilder.Entity<Locker>().HasData(new Locker() { Id = 10004, Name = "L4", LockerKey = "1234", IsActive = true, IsOpen = false, BankId = 102 });
            modelBuilder.Entity<Locker>().HasData(new Locker() { Id = 10005, Name = "L5", LockerKey = "1234", IsActive = true, IsOpen = false, BankId = 103 });
            modelBuilder.Entity<Locker>().HasData(new Locker() { Id = 10006, Name = "L6", LockerKey = "1234", IsActive = true, IsOpen = false, BankId = 101 });
            modelBuilder.Entity<Locker>().HasData(new Locker() { Id = 10007, Name = "L7", LockerKey = "1234", IsActive = true, IsOpen = false, BankId = 101 });
            modelBuilder.Entity<Locker>().HasData(new Locker() { Id = 10008, Name = "L8", LockerKey = "1234", IsActive = true, IsOpen = false, BankId = 102 });
            modelBuilder.Entity<Locker>().HasData(new Locker() { Id = 10009, Name = "L9", LockerKey = "1234", IsActive = true, IsOpen = false, BankId = 102 });
            modelBuilder.Entity<Locker>().HasData(new Locker() { Id = 10010, Name = "L10", LockerKey = "1234", IsActive = true, IsOpen = false, BankId = 103 });

            modelBuilder.Entity<Locker>().HasData(new Locker() { Id = 20001, Name = "L11", LockerKey = "1234", IsActive = true, IsOpen = false, BankId = 201 });
            modelBuilder.Entity<Locker>().HasData(new Locker() { Id = 20002, Name = "L12", LockerKey = "1234", IsActive = true, IsOpen = false, BankId = 201 });
            modelBuilder.Entity<Locker>().HasData(new Locker() { Id = 20003, Name = "L13", LockerKey = "1234", IsActive = true, IsOpen = false, BankId = 202 });
            modelBuilder.Entity<Locker>().HasData(new Locker() { Id = 20004, Name = "L14", LockerKey = "1234", IsActive = true, IsOpen = false, BankId = 201 });
            modelBuilder.Entity<Locker>().HasData(new Locker() { Id = 20005, Name = "L15", LockerKey = "1234", IsActive = true, IsOpen = false, BankId = 201 });
            modelBuilder.Entity<Locker>().HasData(new Locker() { Id = 20006, Name = "L16", LockerKey = "1234", IsActive = true, IsOpen = false, BankId = 202 });
            modelBuilder.Entity<Locker>().HasData(new Locker() { Id = 20007, Name = "L17", LockerKey = "1234", IsActive = true, IsOpen = false, BankId = 201 });
            modelBuilder.Entity<Locker>().HasData(new Locker() { Id = 20008, Name = "L18", LockerKey = "1234", IsActive = true, IsOpen = false, BankId = 202 });

            modelBuilder.Entity<Locker>().HasData(new Locker() { Id = 30001, Name = "L19", LockerKey = "1234", IsActive = true, IsOpen = false, BankId = 301 });
            modelBuilder.Entity<Locker>().HasData(new Locker() { Id = 30002, Name = "L20", LockerKey = "1234", IsActive = true, IsOpen = false, BankId = 302 });
            modelBuilder.Entity<Locker>().HasData(new Locker() { Id = 30003, Name = "L21", LockerKey = "1234", IsActive = true, IsOpen = false, BankId = 302 });
            modelBuilder.Entity<Locker>().HasData(new Locker() { Id = 30004, Name = "L22", LockerKey = "1234", IsActive = true, IsOpen = false, BankId = 301 });
            modelBuilder.Entity<Locker>().HasData(new Locker() { Id = 30005, Name = "L23", LockerKey = "1234", IsActive = true, IsOpen = false, BankId = 302 });
            modelBuilder.Entity<Locker>().HasData(new Locker() { Id = 30006, Name = "L24", LockerKey = "1234", IsActive = true, IsOpen = false, BankId = 302 });
            modelBuilder.Entity<Locker>().HasData(new Locker() { Id = 30007, Name = "L25", LockerKey = "1234", IsActive = true, IsOpen = false, BankId = 302 });
            modelBuilder.Entity<Locker>().HasData(new Locker() { Id = 30008, Name = "L26", LockerKey = "1234", IsActive = true, IsOpen = false, BankId = 302 });


            #endregion
        }

        public void EnsureDbCreated()
        {
            this.Database.EnsureCreated();
        }
    }
}
