﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

using LockerBank.Models;
using LockerBank.Interface;
using LockerBank.Config;

namespace LockerBank.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LockerBankController : ControllerBase
    {
        private IDbContext _lockerBankContext;

        public LockerBankController(IDbContext context)
        {
            this._lockerBankContext = context;
            
        }

        [HttpGet]
        [Route("regions")]
        public async Task<IActionResult> GetRegions()
        {
            _lockerBankContext.EnsureDbCreated();
            
            var regions = _lockerBankContext.Regions.Where(x => x.IsActive == true);

            if (regions.Count() == 0)
            {
                return BadRequest("No active regions available.");
            }
            return await Task.FromResult(Ok(regions.ToList<Region>()));
        }

        [HttpGet]
        [Route("bank/{regionId}")]
        public async Task<IActionResult> GetBanks(int regionId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var banks = _lockerBankContext.Banks
                        .Where(x => x.Region.Id == regionId && 
                               x.IsActive == true && 
                               x.Region.IsActive == true);

            if (banks.Count() == 0)
            {
                var region = await _lockerBankContext.Regions.FindAsync(regionId);
                var inactiveBanks = _lockerBankContext.Banks.Where(x => x.Region.Id == regionId);

                if (region == null)
                    return BadRequest("No such region exists.");

                else if (!region.IsActive)
                    return BadRequest($"Region {region.Name} is inactive.");

                else if (inactiveBanks != null)
                    return BadRequest($"No active locker banks found for the region {region.Name}");
            }

            return Ok(banks.ToList<Bank>());
        }

        [HttpGet]
        [Route("locker/{bankId}")]
        public async Task<IActionResult> GetLockerDetails(int bankId)
        {
            var lockers = _lockerBankContext.Lockers
                                            .Where(x => x.BankId == bankId && 
                                                   x.IsActive == true);

            if (lockers.Count() == 0)
            {
                var bank = await _lockerBankContext.Banks.FindAsync(bankId);

                if (bank == null)
                    return BadRequest("No such locker bank found.");
                else if (!bank.IsActive)
                    return BadRequest($"Locker bank {bank.Name} is inactive.");
                else
                    return BadRequest($"Not active lockers found for the locker bank {bank.Name}.");
            }
            return Ok(lockers.ToList<Locker>());
        }
    }
}