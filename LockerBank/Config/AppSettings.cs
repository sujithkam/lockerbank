﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LockerBank.Config
{
    public class AppSettings
    {
        public ConnectionString ConnectionStrings { get; set; }
    }

    public class ConnectionString
    {
        public string DBConnection { get; set; }
    }
}
