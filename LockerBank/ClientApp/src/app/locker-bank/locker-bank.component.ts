import { Component, Inject, OnInit } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { LockerBankService } from '../services/lockerbankservice.service';

@Component({
  selector: 'locker-bank',
  templateUrl: './locker-bank.component.html'
})
export class LockerBankComponent implements OnInit {
  public lockers: Locker[];
  regionList: Array<any> = [];
  bankList: Array<any> = [];
  
  constructor(private _lockerService: LockerBankService) {
    
  }

  ngOnInit() {
    this._lockerService.getRegionList().subscribe(
      data => this.regionList = data, error => alert(error._body) 
    )
  }

  getLockerBankList(regionId) {
    this.bankList = [];
    this.lockers = [];
    if (regionId == '') return;
    this._lockerService.getLockerBankByRegion(regionId).subscribe(
      data => this.bankList = data, error => alert(error._body)
    )
  }

  getLockerList(bankId) {
    this.lockers = [];
    if (bankId == '') return;
    this._lockerService.getLockerDetailsByBank(bankId).subscribe(
      data => this.lockers = data, error => alert(error._body)
    )
  }
  
  validateKey(currentKey: string, tempKeyCtrl: string) {
    event.preventDefault();
    let ctrl: HTMLInputElement = document.getElementById('key_' + tempKeyCtrl) as HTMLInputElement;
    let tempKey = ctrl.value;
    if (tempKey == currentKey)
      alert('Key is correct.');
    else
      alert('Key is incorrect. Please try again');
  }
}

interface Locker {
  id: number;
  name: string;
  isActive: boolean;
  isOpen: boolean;
  lockerKey: string;
}
