import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { LockerBankService } from './services/lockerbankservice.service';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { LockerBankComponent } from './locker-bank/locker-bank.component';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    LockerBankComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', redirectTo: 'home', pathMatch: 'full' },
      { path: 'home', component: HomeComponent },
      { path: 'locker-bank', component: LockerBankComponent },
      { path: '**', redirectTo: 'home' }
    ])
  ],
  providers: [LockerBankService],
  bootstrap: [AppComponent]
})
export class AppModule { }
