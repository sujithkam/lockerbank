﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LockerBank.Models
{
    public class Locker : BaseModel
    {
        public string LockerKey { get; set; }
        public bool IsOpen { get; set; }

        public int BankId { get; set; }
        public Bank Bank { get; set; }
    }
}
