﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LockerBank.Models
{ 
    public class Bank:BaseModel
    {
        public int RegionId { get; set; }
        public Region Region { get; set; }

        public ICollection<Locker> Lockers { get; set; }        
    }
}
