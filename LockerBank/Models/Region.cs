﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LockerBank.Models
{
    public class Region:BaseModel
    {
        public ICollection<Bank> Banks { get; set; }
    }
}
