The LockerBank project has been created using .NET Core (2.1) for Web API with EntityFramework Core with Code First approach. 

The seed data is created during ‘OnModelCreating’. If the data needs to be updated, we can use SQL Server Object Explorer within Visual Studio and connect to the localdb.

The project uses the local sql server and the db context class is injected using interface in startup.cs. The database name has been set to LockerBank. 

Web API with standard HTTP GET requests have been created with attribute based routing.

Angular has been adopted for the front end display of data.

Code Assumptions : 

Region : The global locations 
Bank : Locker Bank , with each Region having multiple Locker Bank
Locker : Each Locker Bank will consist of multiple Lockers.

Locker has a column LockerKey, a software key based locker that has a default value of 1234. We can update this value and store it in encrypted format. But for demo purpose, this has been not encoded.

We can test this key from the UI where a textbox is provided to enter a key value and validate it against the current key.

XUnit has been adopted for unit tests.
