using System;
using System.Collections.Generic;
using Xunit;
using Microsoft.AspNetCore.Mvc;
using LockerBank.Interface;
using LockerBank.Data;
using LockerBank.Controllers;
using LockerBank.Models;

namespace LockerBank.Test
{
    public class ControllerTests
    {

        private IDbContext _testdbContext;
        private LockerBankController _controller;

        public ControllerTests()
        {
            InitializeDbContext();
        }

        private void InitializeDbContext()
        {
            var optionsBuilder = new Microsoft.EntityFrameworkCore.DbContextOptionsBuilder<LockerBankContext>();
            _testdbContext = new LockerBankContext(optionsBuilder.Options);
            _controller = new LockerBankController(_testdbContext);
        }

        [Fact]
        public void TestForValidatingRegions()
        {
            //arrange data
            _testdbContext.EnsureDbCreated();
            
            //actual
            var regions = _controller.GetRegions().Result as OkObjectResult;
            var regionList = regions.Value as List<Region>;

            Assert.NotNull(regions);
            Assert.NotEmpty(regionList);
        }

        [Fact]
        public void TestForValidatingLockerBanks()
        {
            //arrange data
            _testdbContext.EnsureDbCreated();

            //expected
            Bank expected = new Bank() { Id = 101, Name = "cbd_lockerbank_1", IsActive = true, RegionId = 1 };

            //actual
            var banks = _controller.GetBanks(expected.RegionId).Result as OkObjectResult;
            var bankList = banks.Value as List<Bank>;
            var actual = bankList.Find(x => x.Id == expected.Id);

            Assert.NotNull(banks);
            Assert.Equal(expected.Name, actual.Name);
        }

        [Fact]
        public void TestForInvalidLockerBanks()
        {
            //arrange data
            _testdbContext.EnsureDbCreated();

            //expected
            Bank expected = new Bank() { Id = 101, Name = "cbd_lockerbank_1", IsActive = true, RegionId = 5 };

            //actual
            var banks = _controller.GetBanks(expected.RegionId).Result as BadRequestObjectResult;
            var bankList = banks.Value as List<Bank>;

            Assert.Null(bankList);
        }

        [Fact]
        public void TestForValidLockers()
        {
            //arrange data
            _testdbContext.EnsureDbCreated();

            //expected
            Locker expected = new Locker() { Id = 30001, Name = "L19", LockerKey = "1234", IsActive = true, IsOpen = false, BankId = 301 };
            
            //actual
            var lockers = _controller.GetLockerDetails(expected.BankId).Result as OkObjectResult;
            var lockerList = lockers.Value as List<Locker>;

            var actual = lockerList.Find(x => x.Id == expected.Id);

            Assert.NotNull(lockerList);
            Assert.Equal(expected.Name, actual.Name);
        }

        [Fact]
        public void TestForInvalidLockers()
        {
            //arrange data
            _testdbContext.EnsureDbCreated();

            //expected
            Locker expected = new Locker() { Id = 300010, Name = "L19", LockerKey = "1234", IsActive = true, IsOpen = false, BankId = 3010 };
            var expectedResult = "No such locker bank found.";

            //actual
            var lockers = _controller.GetLockerDetails(expected.BankId).Result as BadRequestObjectResult;
            var actual = lockers.Value as string;

            Assert.Equal(expectedResult, actual);
        }

    }
}
